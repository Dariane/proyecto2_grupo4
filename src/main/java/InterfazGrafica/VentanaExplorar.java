/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazGrafica;

import CraterData.craterData;
import CraterModelo.Crater;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import rovers.Rover;

/**
 *
 * @author idark
 * Ventana de explorar
 * Cuando el usuario da click a Explorar se muestra una nueva ventana en este caso con el fondo de marte que hemos escogido
 */
public class VentanaExplorar {
    
    BorderPane root;
    Rover rover;
    ArrayList<Crater> craters;
    
    public VentanaExplorar() throws FileNotFoundException, IOException{
        mostrarVentana();
    }
    /***
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void mostrarVentana() throws FileNotFoundException, IOException{
        root = new BorderPane();
        
        //Contenedor del mapa
        //Pane
        Pane cntMapa = new Pane();
        //Crear el nuevo rover
        rover = new Rover();
        //El fondo de marte
        ImageView imgvMapa = new ImageView(new Image(new FileInputStream(Constantes.RESOURCE_FOLDER+"LunaRojiza.png")));
        //El size de la imagen de fondo
        imgvMapa.setFitWidth(938); //-> 847
        imgvMapa.setFitHeight(658); //-> 567 
        //agregando el rover y la imagen
        cntMapa.getChildren().addAll(imgvMapa, rover.getImgRover());
        //Cargando la información de crater
        craters = craterData.cargarCrater();
        //recorriendo 
        for(Crater crater: craters){    
            //añadiendo los circulos
            cntMapa.getChildren().add(crater.getCirculo());
            
            crater.getCirculo().setOnMouseClicked(e->{
                Label noml = new Label(crater.toString());
                noml.setPadding(new Insets(10,10,10,10));
                root.setBottom(noml);
            });
        }
        
        //Contenedor de panel de control
        VBox cntControl = new VBox(); 
        //alineando a la izquierda
        cntControl.setAlignment(Pos.TOP_LEFT);
        cntControl.setMinWidth(353);
        cntControl.setSpacing(5);
        //Organización de la ventana explorador los label y txtFields iran del lado derecho
        Label lbComando = new Label("Ingrese Comando: ");
        //En este textfield se ingresa los comandos de avanzar, girar, dirigirse y sensar
        TextField txtfComando = new TextField();
        txtfComando.setMaxWidth(313);
        //Luego en esta parte debajo del label, el textfiled ira mostrando las acciones que ha realizado el rover
        Label lbComandos = new Label("Comandos Ingresados: ");
        TextArea txtaComandos = new TextArea();
        txtaComandos.setMaxWidth(313);
        txtaComandos.setMinHeight(313);
        //Set on Key, aqui estoy separando la informacion que me llega
        /*
        por ejemplo en el caso de avanzar, el rover avanzará normalmente
        en el caso de girar se hace un split en los ":" teniendo en cuenta que lo primero es la palabra girar
        y el segundo sera el valor del giro, el angulo
        
        */
        txtfComando.setOnKeyPressed(e->{ 
            if(KeyCode.ENTER.equals(e.getCode())){
                String[] com  = txtfComando.getText().split(":");
                String comando = com[0].trim();
                //Si se ingresa en el Textfield, el rover avanza
                if(comando.equals("avanzar")){
                    rover.avanzar();
                //gira el robot con respecto a las manecillas del reloj. 
                //Si es que se llega a ingresar un angulo negativo se gira en contra de las manecillas dle reloj
                }else if(comando.equals("girar")){
                    double angulo = Double.parseDouble(com[1].trim());
                    rover.girar(angulo);
                    comando = comando + com[1].trim();
                    //usamos la palabra dirigirse en vez de desplazarse 
                    //dirigirse(posicionx, posiciony)
                    /*
                    Para el caso de dirigirse se ingresa el comando como
                    dirigirse:20,20
                    lo que se hace aqui es separar por medio de "," los valores de X y Y
                    */
                }else if(comando.equals("dirigirse")){
                    String[] dist = com[1].split(",");
                    double X = Double.parseDouble(dist[0].trim());
                    double Y = Double.parseDouble(dist[1].trim());
                    rover.dirigirse(X, Y);
                    //Usamos la palabra sensar en vez de explorar
                    //Una vez que se usa este comando y si el rover se encuentra en un circulo rojo, este se torna azul y
                    //muestra el crater como sensado.
                    /*
                    en el caso de sensar 
                    cuando el rover se encuentre en un crater, o sea en los circulos rojos
                    el usuario al ingresar el comando sensar, generará que este circulo rojo
                    se convierta en un circulo azul, agregando que saldrá en el textfield que 
                    el crater se ha sensado
                    */
                }else if(comando.equals("sensar")){
                    for(Crater crater : craters){
                        if(rover.intersects(crater.getCirculo())){
                            rover.sensar(crater);
                            comando = "Crater Sensado!";
                            crater.setExplorado(true);
                            crater.getCirculo().setFill(Color.BLUE);
                            break;
                        }
                    }
                }
                try{
                    agregarComando(comando);
                    txtaComandos.setText(leerComandos());
                }catch (IOException ex) {//Exception
                    System.out.println(ex.getMessage());
                }
                txtfComando.clear();
            }
        });
        //añadiendo los label y textfield
        cntControl.getChildren().addAll(
                lbComando,
                txtfComando,
                lbComandos,
                txtaComandos
        );
        
       
       root.setLeft(cntMapa);
       root.setRight(cntControl);
       root.setMargin(cntControl, new Insets(20,20,20,20));
    }
        /***
         * 
         * @param string 
         */
    public void enviarComandos(String string){
        
    }
    /***
     * 
     * @return root 
     */
    public BorderPane getRoot(){
        
        
        return root;
    }
    /***
     * 
     * @param comando
     * @throws IOException 
     */
    public static void agregarComando(String comando) throws IOException{
        try(BufferedWriter bf = new BufferedWriter(new FileWriter(Constantes.RESOURCE_FOLDER+"comandos.txt" , true))){
                bf.write(">> "+comando+"\n");
            }
    }
   /***
    * 
    * @return comandos son los comandos que se lee
    * @throws IOException 
    */
    public static String leerComandos() throws IOException{
         String comandos = "";
        try(BufferedReader bf = new BufferedReader(new FileReader(Constantes.RESOURCE_FOLDER+"comandos.txt"))) {
            String linea;
            while((linea=bf.readLine())!=null){
                comandos = comandos + linea + "\n";
            }
        }
        return comandos;
    }
}
