/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazGrafica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author idark
 * Ventana de inicio
 */
public class VentanaInicio {
    
    StackPane root;
    /***
     * 
     * @throws FileNotFoundException 
     */
    public VentanaInicio() throws FileNotFoundException{
        mostrarVentana();
    }
    /***
     * 
     * @throws FileNotFoundException 
     */
    public void mostrarVentana() throws FileNotFoundException{

        root = new StackPane();
        //Fondo de marte
        Image imgFondo = new Image(new FileInputStream(Constantes.RESOURCE_FOLDER + "MarteFondo.jpg"),474,270,false,false);
        ImageView imgvFondo = new ImageView(imgFondo);
        VBox cntBotones = new VBox();
        //Alineando los botones en el centro
        cntBotones.setAlignment(Pos.CENTER);
        cntBotones.setSpacing(20);
        //Boton Explorar
        Button btExplorar = new Button("Explorar");
        btExplorar.setMinWidth(300);
        btExplorar.setOnAction(e -> {
            try{
                abrirExplorar();
            }catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        //Boton Planificar Rutas que te lleva a esa ventana
        Button btPlanificar = new Button("Planificar Rutas");
        btPlanificar.setMinWidth(300);
        btPlanificar.setOnAction(e -> abrirPlanificar());
        //Boton ver reportes que te lleva a la ventana Ver Reportes
        Button btReportes = new Button("Ver Reportes");
        btReportes.setMinWidth(300);
        btReportes.setOnAction(e -> abrirReportes());
        //Boton salir que cierra la pestaña
        Button btSalir = new Button("Salir");
        btSalir.setMinWidth(300);
        btSalir.setOnAction(e -> {Platform.exit();});
       /* 
        Label infoMarte = new Label("Created by:/n Arom, Dariane and Santiago");
        
        infoMarte.setBackground(Color.red);
        infoMarte.setOpaque(true);
        
        infoMarte.setMinWidth(400);*/
        //Agregando los botones
        cntBotones.getChildren().addAll(
                btExplorar,
                btPlanificar,
                btReportes,
                btSalir
              //  infoMarte
        );
        
        root.getChildren().addAll(
                imgvFondo,
                cntBotones
        );
    }
   /***
    * 
    * @throws FileNotFoundException
    * @throws IOException 
    */
    public void abrirExplorar() throws FileNotFoundException, IOException{
        Stage stage = new Stage();
        VentanaExplorar ventanaExplorar = new VentanaExplorar();
        Scene scene = new Scene(ventanaExplorar.getRoot(),1300,677);
        stage.setScene(scene);
        stage.setTitle("Exploración");//Titulo
        stage.show();
        //Llamando al resource folder 
        if(stage.isShowing()){
            new File(Constantes.RESOURCE_FOLDER+"comandos.txt").delete();
        }
    }
    /***
     * @param
     * Abre la planificacion
     */
    public void abrirPlanificar(){
        Stage stage = new Stage();
        VentanaPlanificar ventanaPlanificar = new VentanaPlanificar();
        //tamaño de la ventana
        Scene scene = new Scene(ventanaPlanificar.getRoot(),750,450);
        stage.setScene(scene);
        stage.setTitle("Planificar Rutas");//Titulo
        stage.show();
        if(stage.isShowing()){//Lllamndo a la carpeta 
            new File(Constantes.RESOURCE_FOLDER+"rutas.txt").delete();
        }
    }
      /***
       * @param
       * Abre los reportes
       */
    public void abrirReportes(){
        Stage stage = new Stage();
        //Se abre
        VentanaReporte ventanaReporte = new VentanaReporte();
        //tamañao de la ventana
        Scene scene = new Scene(ventanaReporte.getRoot(),750,450);
        stage.setScene(scene);
        stage.setTitle("Reporte");//Titulo
        stage.show();
    }
         //Salir
    /***
     * 
     * @param stage 
     */
    public void cerrarVentana(Stage stage){
        System.out.println("Cerrar");
        stage.close();
    }
    /***
     * 
     * @return root
     */
    public StackPane getRoot(){
        return root;
    }

    
}
