/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazGrafica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import rovers.Expedicion;

/**
 *
 * @author idark
 * Pues aqui se configura todo lo que tenga que ver con la opcion de reporte
 * Registrar la fecha de inicio y la final, mas los minerales
 */
public class VentanaReporte {

    BorderPane root;
    TextField txtfFechaInicio,txtfFechaFin,txtfMineral;
    ArrayList<Expedicion> expediciones;
    //TableView<Expedicion> tabla;
    
    public VentanaReporte(){
        mostrarVentana();
    }
    
    public void mostrarVentana(){
        root = new BorderPane();
        HBox h0 = new HBox();
        VBox cntFiltro = new VBox(20);//(Spacing)
        cntFiltro.setPrefWidth(400);
        cntFiltro.setAlignment(Pos.TOP_LEFT);//Alineación
        HBox h1 = new HBox();
        HBox h2 = new HBox();
        HBox h3 = new HBox();
        Label lbFechaInicio = new Label("Fecha Inicio ");
        Label lbFechaFin = new Label("Fecha Fin ");
        Label lbMineral = new Label("Mineral");
        lbFechaInicio.setMinWidth(75);
        lbFechaFin.setMinWidth(75);
        lbMineral.setMinWidth(75);
        
        txtfFechaInicio = new TextField();
        txtfFechaInicio.setPromptText("YYYY-MM-DD");//Fecha de inicio
        txtfFechaInicio.setMaxWidth(200);
        txtfFechaFin = new TextField();
        txtfFechaFin.setPromptText("YYYY-MM-DD");//Fecha Final
        txtfFechaFin.setMaxWidth(200);
        txtfMineral = new TextField();
        txtfMineral.setPromptText("Ingrese el Mineral");  //Ingresar los materiales
        txtfMineral.setMaxWidth(200);
        //
        h1.getChildren().addAll(lbFechaInicio, txtfFechaInicio);
        h2.getChildren().addAll(lbFechaFin, txtfFechaFin);
        h3.getChildren().addAll(lbMineral, txtfMineral);
        BorderPane botonPane = new BorderPane();
        Button bfiltrar = new Button("Filtrar");
        botonPane.setCenter(bfiltrar);
        //añadiendo
        cntFiltro.getChildren().addAll(
                h1,h2,h3
        );
        h0.getChildren().addAll(cntFiltro,botonPane);
        //accion del boton filtrar
        bfiltrar.setOnAction(e->{
            try {
                LeerExpediciones();
                ArrayList lista = Filtrar(expediciones);
                root.setCenter(GenerarTabla(lista));
                
            } catch (IOException ex) {
                root.getChildren().add(new Label("Hubo un problema, lo estamos solucionando..."));
            }
        });      
        
        root.setTop(h0);
        root.setMargin(h0,new Insets(10,10,10,10));//Margen(arriba,derecha,abajo,izquierda)
    }
    
    public Parent getRoot() {
        return root;
    }
    //Leer Expediciones
    public void LeerExpediciones() throws IOException{
        expediciones = new ArrayList<>();
        try(BufferedReader bf = new BufferedReader(new FileReader(Constantes.RESOURCE_FOLDER+"exploraciones.txt"))) {
            String linea = "";
            while((linea=bf.readLine())!=null){
                String[] texto = linea.split(";");
                expediciones.add(new Expedicion(texto[0],texto[1],texto[2]));
            }
        }
    }
    //Array List de Filtrar
    public ArrayList<Expedicion> Filtrar(ArrayList<Expedicion> expediciones_lista){
        expediciones = new ArrayList<Expedicion>();
        for(Expedicion e: expediciones_lista){
            System.out.println(e.getNombre());
            //por lo que estuve probado el textfield nunca es igual a null por lo menos en este caso
            if(txtfFechaInicio.getText().equals(e.getFecha())){
                System.out.println("Filtra por fecha");
                expediciones.add(e);
            }else if(txtfFechaFin.getText().equals(e.getFecha())){
                System.out.println("Filtra por fecha final");
                expediciones.add(e);
            }else if(txtfMineral.getText().equals(e.getMinerales())){
                System.out.println("Filtra por mineral");
                expediciones.add(e);
            }else if(txtfFechaInicio.getText().equals(e.getFecha()) && txtfFechaFin.getText().equals(e.getFecha())){
                System.out.println("Filtra por fecha inicio y fecha final");
                expediciones.add(e);
            }else if(txtfFechaInicio.getText().equals(e.getFecha()) && txtfMineral.getText().equals(e.getMinerales())){
                System.out.println("Filtra por fecha inicio y fecha final");
                expediciones.add(e);
            }else if(txtfFechaFin.getText().equals(e.getFecha()) && txtfMineral.getText().equals(e.getMinerales())){
                System.out.println("Filtra por fecha inicio y fecha final");
                expediciones.add(e);
            }else if(txtfFechaInicio.getText().equals(e.getFecha()) && txtfFechaFin.getText().equals(e.getFecha()) && txtfMineral.getText().equals(e.getMinerales())){
                System.out.println("Filtra por fecha inicio, fecha final y mineral");
                expediciones.add(e);
            }else if(txtfFechaInicio.getText() == null && txtfFechaFin.getText() == null && txtfMineral.getText() == null){
                System.out.println("No filtrar");
                expediciones.add(e);
            }else{
                System.out.println("no se puede filtrar");
                System.out.println(txtfFechaInicio.getText());
            }
        }
        
        return expediciones;
    }
    //Generar la tabla que solicita el ejercicio
    /***
     * 
     * @param lista la lista que se usa para generar la tabla
     */
    public TableView GenerarTabla(ArrayList<Expedicion> lista){
        TableView tabla = new TableView<>();
        System.out.println(lista);
        ObservableList<Expedicion> listaTabla = FXCollections.observableArrayList(lista);
        System.out.println("Hace esto observablelist");
        System.out.println(listaTabla);        
        
        tabla.setEditable(true);
        //Columnas de la tabala
        //Fecha de Exploracion - Nombre - Minerales
        TableColumn<Expedicion, String> colFInicio = new TableColumn<>("Fecha Exploración");
        TableColumn<Expedicion, String> colNombre = new TableColumn<>("Nombre");
        TableColumn<Expedicion, String> colMinerales = new TableColumn<>("Minerales");
        colFInicio.setMinWidth(250);
        colFInicio.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        colNombre.setMinWidth(250);
        colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colMinerales.setMinWidth(250);
        colMinerales.setCellValueFactory(new PropertyValueFactory<>("minerales"));
        
        tabla.getColumns().addAll(colFInicio, colNombre, colMinerales);
        tabla.setItems(listaTabla);
        return tabla;
    }
}
