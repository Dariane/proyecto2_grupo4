/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfazGrafica;

import CraterData.craterData;
import CraterModelo.Crater;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import rovers.Rover;

/**
 * Ventana Planificar Ruta
 * @author idark
 * Ventana donde se planifica la ruta 
 * En esta opción el usuario debe poder ingresar los nombres de los rovers que está interesado en
 *   explorar y la aplicación debe mostrar en qué orden deben ser visitados optimizando la ruta.
 */
public class VentanaPlanificar {

    VBox root;
    TextField txtCrateres;
    TextArea txtRuta;
    ArrayList<Crater> dist_craters;
    /***
     * 
     */
    public VentanaPlanificar(){
        mostrarVentana();
    }
    /***
     * @param mostrarVentana() todo lo que conforma esta ventana
     */
    public void mostrarVentana(){
        root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setMinWidth(353);
        root.setSpacing(5);
        Label lbCrateres = new Label("Crateres");
        txtCrateres = new TextField();
        txtCrateres.setMaxWidth(313);
        Label lbRuta = new Label("Ruta: ");
        txtRuta = new TextArea();
        txtRuta.setMaxWidth(313);
        txtRuta.setMinHeight(313);
        
        txtCrateres.setOnKeyPressed(e ->{
            if(KeyCode.ENTER.equals(e.getCode())){
                try {
                    crearRuta(txtCrateres.getText());
                    VisitarRunnable vr = new VisitarRunnable();
                    Thread t = new Thread(vr);
                    t.start();
                    txtCrateres.clear();
                    //excepcion
                } catch (IOException ex) {
                    txtRuta.setText("Hubo un problema, lo estamos resolviendo!");
                }
            }
        });
        //Agregando 
        root.getChildren().addAll(
                lbCrateres,
                txtCrateres,
                lbRuta,
                txtRuta);
    }
    //Crear Ruta
    /***
     * 
     * @param texto es lo que el usuario ingresa, los diferentes crateres que desea visitar
     * @return dist_crateres distancia de los crateres
     * @throws IOException 
     */
    public ArrayList<Crater> crearRuta(String texto) throws IOException{
        String[] craters = texto.split(",");
        ArrayList<Crater> lista_craters = craterData.cargarCrater();
        dist_craters = new ArrayList<>();
        int n = 0;
        for(String nombre: craters){//recorriendo la lista de crateres
            for(Crater c1: lista_craters){
                if(c1.getNombreCrater().toUpperCase().equals(nombre.trim().toUpperCase())){
                    if(dist_craters.size() == 0){
                        dist_craters.add(n, c1);
                    }else{
                        boolean variable = true;
                        for(Crater c2: dist_craters){
                            if(distanciah(c1.getLatitud_X(),c1.getLongitud_Y()) < distanciah(c2.getLatitud_X(), c2.getLongitud_Y())){
                                dist_craters.add(n, c1);
                                variable = false;
                                break;
                            }
                            n++;
                        }
                        if(variable){
                            dist_craters.add(c1);
                        }
                        variable = true;
                    }
                }
            }
            n = 0;
        }
        int no = 1;
        for(Crater c: dist_craters){
            String nombre = no +". "+ c.getNombreCrater() + "\n";
            EscribirRuta(nombre, true);
            no++;
        }
        txtRuta.setText(LeerRutas());
        
        return dist_craters;
    }
    
    Parent getRoot() {
        return root;
    }
    //Distancia
    /***
     * 
     * @param X distancia X 
     * @param Y distancia Y
     * @return hipotenisa (Teorema de Pitagoras)
     */
    public double distanciah(double X, double Y){
        double hipotenusa = Math.sqrt(Math.pow(X, 2) + Math.pow(Y, 2));
        return hipotenusa;
    }
    //Escribir la ruta
    /***
     * 
     * @param nombre_crater nombre del crater
     * @param variable 
     * @throws IOException 
     */
    public static void EscribirRuta(String nombre_crater, boolean variable) throws IOException{
        try(BufferedWriter bf = new BufferedWriter(new FileWriter(Constantes.RESOURCE_FOLDER+"rutas.txt" , variable))){
                bf.write(nombre_crater);
            }
    }
    //Leer la ruta
    /***
     * 
     * @return rutas 
     * @throws IOException 
     */
    public static String LeerRutas() throws IOException{
         String rutas = "";
        try(BufferedReader bf = new BufferedReader(new FileReader(Constantes.RESOURCE_FOLDER+"rutas.txt"))) {
            String linea;
            while((linea=bf.readLine())!=null){
                rutas = rutas + linea + "\n";
            }
        }
        return rutas;
    }
    
    //Runnable
    /***
     * 
     */
    class VisitarRunnable implements Runnable{
        public void run(){
            
            Platform.runLater(()->{
            try {
                Thread.sleep(1000);//1 segundo
                txtRuta.clear();
                ArrayList<Crater> clon = new ArrayList<>();//crea un nuevo arraylist
                for(Crater cln: dist_craters){//recorre 
                    clon.add(cln);//añade a la nueva lista
                }
                EscribirRuta("",false);
                Rover rover = new Rover();//nuevo rover
                int n = 0;
                
                for(Crater c: dist_craters){
                    EscribirRuta("\n",true);
                    rover.dirigirse(c.getLatitud_X(), c.getLongitud_Y());
                    rover.sensar(c);
                    
                    
                    int no = 1;
                    for(Crater c1: clon){
                        if(c1.equals(c)){
                            String nombre = no +". "+ c1.getNombreCrater() + ", CRATER VISITADO!\n";
                            EscribirRuta(nombre, true);
                            no++;
                        }else{
                            String nombre = no +". "+ c1.getNombreCrater() + ", POR VISITAR\n";
                            EscribirRuta(nombre, true);
                            no++;
                        }
                    }
                    
                    txtRuta.setText(LeerRutas());
                    Thread.sleep(500);    
                    
                    no = 1;
                    clon.remove(n);
                }
                //Excepciones
            } catch (FileNotFoundException | InterruptedException ex) {
                System.out.println("Hubo un problema, lo estamos resolviendo");
            } catch (IOException ex) {
                System.out.println("Hubo un problema, lo estamos resolviendo");
            }
            });
        }
    }
    
}
