/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CraterModelo;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Dariane Medranda
 * @param Crater recibe idcrater, nombrecrater, radio, latitud_x, latitud_Y , respectivos Getters y Setters
 * 
 */
public class Crater {
    //Variables
    private String idcrater;
    private String nombrecrater;
    private double radio;
    private double latitud_X;
    private double longitud_Y;
    private String informacion;
    private Color color = Color.RED;
    private Circle circulo;
    private boolean explorado = false;
    //Constructor Crater
    public Crater(String idcrater, String nombrecrater, double radio, double latitud_X, double longitud_Y){
        this.idcrater = idcrater;
        this.nombrecrater = nombrecrater;
        this.radio = radio;
        this.latitud_X = latitud_X;
        this.longitud_Y = longitud_Y;
        circulo = new Circle(latitud_X, longitud_Y, radio, color);
        circulo.setOpacity(0.5);
        circulo.setStrokeWidth(2);
    }
    //Getters y Setters
     public String getIdCrater() {
        return idcrater;
    }
    public void setIdCrater(String idcrater) {
        this.idcrater = idcrater;
    }
    public String getNombreCrater(){
        return nombrecrater;
    }
    public void setNombreCrate(String nombrecrater){
        this.nombrecrater = nombrecrater;
    }
    public double getRadio(){
        return radio;
    }
    public void setRadio(double radio){
        this.radio = radio;
    }
    public double getLatitud_X(){
        return latitud_X;
    }
    public void setLatitud_X(double latitud_X){
        this.latitud_X = latitud_X;
    }
    public double getLongitud_Y(){
        return longitud_Y;
    }
    public void setLongitud_X(double longitud_Y){
        this.longitud_Y = longitud_Y;
    }
    public String getInformacion(){
        return this.informacion;
    }
    public void setInformacion(String info){
        this.informacion = info;
    }
    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public Circle getCirculo() {
        return circulo;
    }
    public void setCirculo(Circle circulo) {
        this.circulo = circulo;
    }
    public boolean isExplorado() {
        return explorado;
    }
    public void setExplorado(boolean explorado) {
        this.explorado = explorado;
    } 
    
    public String toString(){
        if(this.informacion != null){
            return this.nombrecrater +"\n"+ this.informacion;
        }else{
            return this.nombrecrater;
        }
    }
}
