package com.mycompany.proyectosegundoparcial;

import InterfazGrafica.VentanaInicio;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws FileNotFoundException {
        
        VentanaInicio ventanaInicio = new VentanaInicio();
        Scene sc = new Scene(ventanaInicio.getRoot(),473,269);
        stage.setScene(sc);
        stage.setTitle("NASAgame");
        stage.show();     
    }

    public static void main(String[] args) {
        launch();
    }

}