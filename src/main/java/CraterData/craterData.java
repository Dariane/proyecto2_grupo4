/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CraterData;

import CraterModelo.Crater;
import InterfazGrafica.Constantes;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Dariane Medranda
 * 
 */
public class craterData {
    private static String TXT = Constantes.RESOURCE_FOLDER+"crateres_info.txt";
    
    /**
     * @param ArrayList<Crater> Este arraylist es el de cargar crater
     * 
     * @return crater regresa los crateres que hay en marte
     * @throws IOException Excepcion utilizada
     */
    
    //ArrayList de donde se sacan los crateres
    public static ArrayList<Crater> cargarCrater() throws IOException{
        ArrayList<Crater> crater = new ArrayList<>();
        try(BufferedReader bf = new BufferedReader(new FileReader(TXT))) {
            String linea;
            while((linea=bf.readLine())!=null){
                String c[]=linea.split(",");
                crater.add(new Crater(c[0].trim(), c[1].trim(), Double.parseDouble(c[4].trim()), 
                        Double.parseDouble(c[2].trim()), Double.parseDouble(c[3].trim())));
            }
        }
        return crater;
    }//Comentario
}

