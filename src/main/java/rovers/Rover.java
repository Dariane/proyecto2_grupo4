/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rovers;

import CraterModelo.Crater;
import InterfazGrafica.Constantes;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
//import rovers.Rover.dirigirse.ComandoRunnable;

/**
 *
 * @author Dariane Medranda
 * acciones del rover
 */
public class Rover {
    ImageView imgRover;
    double angulo,hipotenusa,vector_x2,vector_y2;
    /***
     * 
     * @throws FileNotFoundException 
     */
    public Rover() throws FileNotFoundException{
        imgRover = new ImageView(new Image(new FileInputStream(Constantes.RESOURCE_FOLDER+"rovers.png")));
        imgRover.setFitWidth(44);
        imgRover.setFitHeight(43);
    }
    /***
     * 
     * @param rover 
     */
    public Rover(ImageView rover){
        this.imgRover = rover;
    }
    //Calculo de cuanto avanzará el rover
    /***
     * 
     */
    public void avanzar(){
        imgRover.setLayoutY(Constantes.DISTANCIA * Math.sin(Math.toRadians(imgRover.getRotate())) + imgRover.getLayoutY());
        imgRover.setLayoutX(Constantes.DISTANCIA * Math.cos(Math.toRadians(imgRover.getRotate())) + imgRover.getLayoutX());
    }
    //Giro del rover
    /***
     * 
     * @param angulo 
     */
    public void girar(double angulo){
        if(imgRover.getRotate() + angulo  <= 360){
            imgRover.setRotate(angulo + imgRover.getRotate() - 360);
        }else{
            imgRover.setRotate(angulo + imgRover.getRotate());
        }
    }
    //Como avanzará el rover cuando se le ingrese las coordenadas x y y
    /***
     * 
     * @param X
     * @param Y 
     */
    public void dirigirse(double X, double Y){
        double vector_x1 = Constantes.DISTANCIA * Math.cos(Math.toRadians(imgRover.getRotate()));
        double vector_y1 = Constantes.DISTANCIA * Math.sin(Math.toRadians(imgRover.getRotate()));
        double pendiente1 = vector_y1 / vector_x1;
        
        vector_x2 = X - imgRover.getLayoutX();
        vector_y2 = Y - imgRover.getLayoutY();
        
        //para calcular el angulo que debe rotar
        angulo = Math.toDegrees(Math.acos((vector_x1 * vector_x2 + vector_y1 * vector_y2)/
                        (Math.sqrt(Math.pow(vector_x1, 2) + Math.pow(vector_y1, 2)) * 
                        (Math.sqrt(Math.pow(vector_x2, 2) + Math.pow(vector_y2, 2))))));
        
        //con la pendiente determinaremos el signo del angulo
        if(Y/pendiente1 < Y){
            angulo = angulo * -1;
        }
        
        ComandoRunnable cr = new ComandoRunnable();
        Thread t = new Thread(cr);
        t.start();
        
    }
    //Que hará el rover cuando se le pida sensar un crater
    /***
     * 
     * @param c 
     */
    public void sensar(Crater c){
        String senso = "";
        LocalDateTime dateTime = LocalDateTime.now();
        //formato en año-mes-dia
        String formatDateTime = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")); 
        ArrayList<String> elementos = new ArrayList<>();
        //ELEMENTOS DEL PLANETA MARTE
        elementos.add("OXIGENO"); 
        elementos.add("NITROGENO"); 
        elementos.add("CARBONO"); 
        elementos.add("HIDROGENO"); 
        elementos.add("ARGON");
        elementos.add("CRIPTON");
        elementos.add("METANO");
        String elemts = "";
        int cantidad = (int)(Math.random()*7+1);
        for(int i = 0; i < cantidad; i++){
            int posicion = (int)(Math.random()*7+1);
            elemts = elemts + elementos.get(posicion) + ",";
            c.setInformacion(elemts);
        }
        senso = formatDateTime +";"+ c.getNombreCrater() + ";" + elemts;
        try {
            Escribir_Exploraciones(senso);
        } catch (IOException ex) {
            System.out.println("Hubo un error, estamos manejandolo");
        }
    }
        /***
         * 
         * @param circulo
         * @return 
         */
    public boolean intersects(Circle circulo){
        if(imgRover.getBoundsInParent().intersects(circulo.getBoundsInParent())){
            return true;
        }else{
            return false;
        }
    }
    //Getters
    public String getX(){
        return String.valueOf(imgRover.getLayoutX());
    }
    public String getY(){
        return String.valueOf(imgRover.getLayoutY());
    }
    public ImageView getImgRover() {
        return imgRover;
    }
    
    //Runnable
    class ComandoRunnable implements Runnable{
        @Override
        public void run() {
            hipotenusa = Math.sqrt(Math.pow(vector_x2, 2) + Math.pow(vector_y2, 2));
            try {
                Platform.runLater(()->{
                    girar(angulo);
                });
                for(int i = 0; i < hipotenusa/Constantes.DISTANCIA; i++){
                    Thread.sleep(500);    
                    Platform.runLater(()->{
                        avanzar();
                    });
                }
            } catch (InterruptedException ex) {
                System.out.println("Hubo un problema, lo estamos resolviendo");
            }
        }      
    }
    //Escribir las exploraciones hechas
    /***
     * 
     * @param expedicion
     * @throws IOException 
     */
    public static void Escribir_Exploraciones(String expedicion) throws IOException{
        try(BufferedWriter bf = new BufferedWriter(new FileWriter(Constantes.RESOURCE_FOLDER+"exploraciones.txt" , true))){
                bf.write(expedicion+"\n");
            }
    }
}