/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rovers;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Dariane Medranda
 */
public class Expedicion {
    private String fecha;
    private String nombre;
    private String minerales;
    
    public Expedicion(String fecha, String nombre, String minerales) {
        this.fecha = fecha;
        this.nombre = nombre;
        this.minerales = minerales.substring(0, minerales.length()-1);
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMinerales() {
        return minerales;
    }

    public void setMinerales(String minerales) {
        this.minerales = minerales;
    }
    

}
